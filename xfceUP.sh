#!/bin/bash
### a script to build Xfce from git using Arch Linux's AUR
### Note: a number of packages either aren't available in the AUR as
###		git builds, or error during building. You should check every
###		once in a while and verify the list and status of these packages.
###
### XFCE_CORE - core Xfce components
### XFCE_APPS - Xfce applications
### XFCE_PLUGS - xfce4-panel plugins
### THUNAR_PLUG - thunar plugins
### BINDINGS - Xfce bindings  

### globals
URL="https://aur.archlinux.org/cgit/aur.git/snapshot/"
EXT=".tar.gz"
DIR="$HOME/tmp"

CACHE="$HOME/.cache/commits.log"
LAST="$HOME/.cache/last"
MESSAGEFILE="$HOME/.cache/messages"

### create or empty dir on start
[[ -d "$DIR" ]] && rm -rf "$DIR"/* || mkdir -p $DIR
cd "$DIR"

### ala Palpatine, do the building using makepkg
do_it () {
	for file in $1
	do
		 wget "$URL$file$EXT"
		 tar xzvf "$file$EXT"
		 cd "$file"
		 makepkg -si --noconfirm
		 cd ..
#		 rm "$file$EXT"
#		 rm -rf "$file"
	done
}

### Xfce Core Components
XFCE_CORE=" xfce4-dev-tools-git
		libxfce4util-git
		xfconf-git
		libxfce4ui-git
		garcon-git
		exo-git
		xfce4-panel-git
		thunar-git
		xfce4-settings-git
		xfce4-session-git
		xfwm4-git
		xfdesktop-git
		xfce4-appfinder-git
		tumbler-git
		thunar-volman-git
		xfce4-power-manager-git"
# deprecated: libxfcegui4 gtk-xfce-engine
# no git: 
# error: 

### Xfce Applications
XFCE_APPS=" mousepad-git
		orage-git
		parole-git
		ristretto-git
		squeeze-git
		xfburn-git
		xfce4-notifyd-git
		xfce4-panel-profiles-git
		xfce4-screenshooter-git
		xfce4-taskmanager-git
		xfce4-terminal-git
		xfce4-volumed-pulse-git
		xfdashboard-git"
# deprecated:
# no git: catfish xfce4-dict xfce4-mixer xfce4-screensaver
# error: gigolo-git xfmpc-git

### Xfce Panel Plugins
XFCE_PLUGS="xfce4-clipman-plugin-git
		xfce4-cpufreq-plugin-git
		xfce4-datetime-plugin-git
		xfce4-diskperf-plugin-git
		xfce4-fsguard-plugin-git
		xfce4-generic-slider-git
		xfce4-genmon-plugin-git
		xfce4-indicator-plugin-git
		xfce4-netload-plugin-git
		xfce4-pulseaudio-plugin-git
		xfce4-smartbookmark-plugin-git
		xfce4-systemload-plugin-git
		xfce4-weather-plugin-git
		xfce4-whiskermenu-plugin-git
		xfce4-xkb-plugin-git"
# deprecated:
# no git: xfce4-battery-plugin xfce4-calculator-plugin xfce4-cpugraph-plugin xfce4-embed-plugin
#    xfce4-eyes-plugin xfce4-hardware-monitor-plugin xfce4-mailwatch-plugin xfce4-mount-plugin
#    xfce4-mpc-plugin xfce4-notes-plugin xfce4-places-plugin xfce4-sample-plugin
#    xfce4-sensors-plugin xfce4-statusnotifier-plugin xfce4-stopwatch-plugin
#    xfce4-time-out-plugin xfce4-timer-plugin xfce4-verve-plugin xfce4-wavelan-plugin
# error:

### Thunar Plugins
THUNAR_PLUG="thunar-archive-plugin-git
		thunar-media-tags-plugin-git
		thunar-shares-plugin-git
		thunar-vcs-plugin-git"
# deprecated:
# no git:
# error:

### Xfce Bindings
BINDINGS=""
# deprecated:
# no git: thunarx-python
# error: xfce4-vala-git

# show latest commits
if [ "$(cat ~/.cache/last)" != "$(cat ~/.cache/last.in)" ]; then
	cat ~/.cache/messages | yad --window-icon=$HOME/.icons/xfce4icon.png --text-info --width 750 --height 300 --title "Xfce git Updater - Xfce Commits Available" &
fi

# prompt for parameters
clear
echo "---------====== Xfce Updater ======---------"
echo
echo "This script will refresh Xfce's AUR git packages."
echo 
echo " - To list the available pacakges, enter 'list' and press the Enter key."
echo 
echo " - To update all of them, press the Enter key."
echo
echo " - To update individual packages or package groups (XFCE_CORE, XFCE_APPS, XFCE_PLUGS, THUNAR_PLUG, BINDINGS), enter a space-separated list and press enter"
echo 
echo " - To cancel this script and exit, enter 'cancel'"
echo
read -p 'Package list: ' INLIST

if [ "$INLIST" == "cancel" ]; then 
 echo
 echo "---------====== Xfce Updater ======---------"
 echo "Request cancelled"
 echo
 exit 1
elif [ "$INLIST" == "list" ]; then 
 echo
 echo "---------====== Xfce Updater ======---------"
 echo "Available packages:"
 echo "XFCE_CORE = $XFCE_CORE"
 echo "XFCE_APPS = $XFCE_APPS"
 echo "XFCE_PLUGS = $XFCE_PLUGS"
 echo "THUNAR_PLUG = $THUNAR_PLUG"
 echo "BINDINGS = $BINDINGS"
 echo
 exit 0
elif [ "$INLIST" != "" ]; then
 for i in $INLIST; do
  if [[ "$i" =~ "XFCE_CORE" ]]; then do_it "$XFCE_CORE"
  elif [[ "$i" =~ "XFCE_APPS" ]]; then do_it "$XFCE_APPS"
  elif [[ "$i" =~ "XFCE_PLUGS" ]]; then do_it "$XFCE_PLUGS"
  elif [[ "$i" =~ "THUNAR_PLUG" ]]; then do_it "$THUNAR_PLUG"
  elif [[ "$i" =~ "BINDINGS" ]]; then do_it "$BINDINGS"
  else NEW_LIST="$NEW_LIST $i"
  fi
 done
 do_it "$NEW_LIST"
 echo
 echo "--------========   Update  Complete   ========--------"
 echo "$INLIST updated"
 echo
 echo "Press Enter to close this window"
 read
else
 do_it "$XFCE_CORE"
 do_it "$XFCE_APPS"
 do_it "$XFCE_PLUGS"
 do_it "$THUNAR_PLUG"
 do_it "$BINDINGS"
 echo
echo "--------========   Update  Complete   ========--------"
echo "All packages updated. Press Enter to close this window"
read
fi

# save last commit from this run
cat $LAST.in > $LAST
#sed -i 's/xupdates/xfce4icon/' /home/xfce/Desktop/Xfce\ Git\ Updater.desktop

exit 0

